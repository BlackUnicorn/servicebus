﻿using System.Threading.Tasks;
using Messages;
using NServiceBus;
using NServiceBus.Logging;

namespace Shipping
{
    public class ShippingPolicy : Saga<ShippingPolicyData>, IAmStartedByMessages<OrderBilled>, IAmStartedByMessages<OrderPlaced>
    {
        private static ILog log = LogManager.GetLogger<ShippingPolicy>();

        public Task Handle(OrderBilled message, IMessageHandlerContext context)
        {
            Data.IsOrderBilled = true;
            log.Info($"Received OrderBilled, OrderId = { message.OrderId}- shipping stuff ");
            return ProcessOrder(context);
        }

        public Task Handle(OrderPlaced message, IMessageHandlerContext context)
        {
            Data.IsOrderPlaced = true;
            log.Info($"Received OrderPlaced, OrderId = {message.OrderId} - Sending stuff...");
            return ProcessOrder(context);
        }

        protected override void ConfigureHowToFindSaga(SagaPropertyMapper<ShippingPolicyData> mapper)
        {
            mapper.ConfigureMapping<OrderPlaced>(message => message.OrderId)
                .ToSaga(sagaData => sagaData.OrderId);
            mapper.ConfigureMapping<OrderBilled>(message => message.OrderId)
                .ToSaga(sagaData => sagaData.OrderId);
        }

        private async Task ProcessOrder(IMessageHandlerContext context)
        {
            if (Data.IsOrderPlaced && Data.IsOrderBilled)
            {
                await context.SendLocal(new ShipOrder() { OrderId = Data.OrderId });
                MarkAsComplete();
            }
        }
    }
}